extends KinematicBody

var zmienna_predkosci = Vector3(0,0,0)

func _ready():
	pass

func _physics_process(delta):
	if Input.is_action_pressed("ui_right"):
		zmienna_predkosci.x += 4
	if Input.is_action_pressed("ui_left"):
		zmienna_predkosci.x -= 4
	if Input.is_action_pressed("ui_up"):
		zmienna_predkosci.z -= 4
	if Input.is_action_pressed("ui_down"):
		zmienna_predkosci.z += 4
	if Input.is_action_just_pressed("ui_accept"):
		zmienna_predkosci.y += 4
		
	move_and_slide(zmienna_predkosci)
	zmienna_predkosci.x = 0
	zmienna_predkosci.y = 0
	zmienna_predkosci.z = 0
